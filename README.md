# lisp

Family of computer programming languages. https://en.m.wikipedia.org/wiki/Lisp_(programming_language)

# Books
* [*LISP 1.5 Programmer's Manual*
  ](https://worldcat.org/search?q=ti%3ALISP+1.5+Programmer%27s+Manual)
  1962, 1967, 1969, 1972, 1973 John MacCarthy
  * [MIT Press](https://mitpress.mit.edu/books/lisp-15-programmers-manual)

# Typed lisps
* [The Typed Racket Guide](https://docs.racket-lang.org/ts-guide/)
* [Typed Clojure](https://typedclojure.org/)

# Libraries
## Parser combinator
* [parser combinator library lisp](https://google.com/search?q=parser+combinator+library+lisp)

# Implementations
## In Rust
* [lisp](https://crates.io/search?q=lisp) @crates.io
